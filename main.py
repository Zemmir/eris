from PyQt5 import uic, QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import *

from functools import partial
from network import networkmanager
import logging, sys, data
from threads import Whitelist_th

logger = logging.getLogger('guardian')
logger.propagate = False
logger.setLevel(logging.INFO)
if not logger.handlers:
    print('yesi')
    fh = logging.FileHandler(filename='history.log')
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
logger.info('Startup.')

class extra_template():
    def reset(self, temp):
        temp.findChild(QtWidgets.QPushButton, 'btn1').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn2').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn3').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn4').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn5').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn6').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn7').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn8').hide()

class extra_tokencheck():
    def check(self, token):
        conn = networkmanager.Cloud(token)
        if conn.check_connection():
            if conn.check_token():
                logger.info('Token is good and cloud is up')
                return True
            else:
                logger.error('Token is bad but cloud is up')
                return False
        else:
            logger.error('Cloud is down')
            return False


class Base(QtWidgets.QDialog):
    def __init__(self):
        super(Base, self).__init__()
        
        #ipsync()
        
        #token = data.read_file().get('token')
        #if token:
        #    global status_txt
        #    status_txt = token_check(token)
            
        self.menu_main = menu_MainPage()
        self.menu_main.show()

class menu_MainPage(QtWidgets.QDialog):
    def __init__(self):
        super(menu_MainPage, self).__init__()

        uic.loadUi('main.ui', self)

        self.findChild(QtWidgets.QPushButton, 'solo').clicked.connect(partial(self.btn_func, 'solo'))
        self.findChild(QtWidgets.QPushButton, 'whitelist').clicked.connect(partial(self.btn_func, 'whitelist'))
        self.findChild(QtWidgets.QPushButton, 'blacklist').clicked.connect(partial(self.btn_func, 'blacklist'))
        self.findChild(QtWidgets.QPushButton, 'autowhitelist').clicked.connect(partial(self.btn_func, 'autowhitelist'))
        self.findChild(QtWidgets.QPushButton, 'newsession').clicked.connect(partial(self.btn_func, 'newsession'))
        self.findChild(QtWidgets.QPushButton, 'kick').clicked.connect(partial(self.btn_func, 'kick'))
        self.findChild(QtWidgets.QPushButton, 'settings').clicked.connect(partial(self.btn_func, 'settings'))
        self.findChild(QtWidgets.QPushButton, 'quit').clicked.connect(partial(self.btn_func, 'quit'))
        self.txtbtm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        
        self.txtbtm.setText("Main Menu")

        
        self.show()
    def btn_func(self, btn):
        # This is executed when the button is pressed
        if btn == "quit":
            #self.wl_thread.__del__()
            self.close()
        elif btn == "solo":
            self.menu = menu_Solo()
            self.menu.show()
            self.close()
            #self.wl_thread.__del__()
        elif btn == "whitelist":
            logger.info("whitelist")
            #self.wl_thread.start()
        elif btn == "settings":
            self.menu = menu_settings()
            self.menu.show()
            self.close()
# Solo menu
class menu_Solo(QtWidgets.QDialog):
    def __init__(self):
        super(menu_Solo, self).__init__()
        uic.loadUi('template.ui', self)
        extra_template.reset(self, self)

        self.btn_stop = self.findChild(QtWidgets.QPushButton, 'btn1')
        self.btn_reload = self.findChild(QtWidgets.QPushButton, 'btn2')
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.txt_top = self.findChild(QtWidgets.QLabel, 'txttop')

        self.btn_reload.show()
        self.btn_stop.show()
        self.txt_btm.show()
        self.txt_top.show()

        # adding by emitting signal in different thread
        self.wl_thread = Whitelist_th()
        self.wl_thread.start()
        self.wl_thread.started.connect(self.solo_started)
        
        # Define function to do on clicks
        self.btn_stop.clicked.connect(self.stop)
        self.btn_reload.clicked.connect(self.solo_started)

        self.btn_stop.setText('Stop')
        self.btn_reload.setText('Reload UI')
        self.txt_btm.setText('Solo Session')

        self.txt_top.setText('Starting Solo')
        self.txt_top.setStyleSheet('color: yellow')

        

    def solo_started(self):
        self.txt_top.setText('Solo Started')
        self.txt_top.setStyleSheet('color: lime')
    
    def stop(self):
        self.txt_top.setText('Stoping Solo')
        self.txt_top.setStyleSheet('color: red')
        self.wl_thread.stop()
        self.menu_mainmenu = menu_MainPage()
        self.menu_mainmenu.show()
        self.close()
# Settings menu
class menu_settings(QtWidgets.QDialog):
    def __init__(self):
        super(menu_settings, self).__init__()
        uic.loadUi('template.ui', self)
        extra_template.reset(self, self)
        #define buttons
        btn_token = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_lists = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        # Show btns
        btn_back.show()
        btn_token.show()
        btn_lists.show()

        # Set text on Btns and btm text
        btn_token.setText('Token')
        btn_lists.setText('Lists')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Settings')

        # Define function to do on click
        btn_back.clicked.connect(self.back)
        btn_token.clicked.connect(self.dialog_token)
    def back(self):
        self.menu_mainmenu = menu_MainPage()
        self.menu_mainmenu.show()
        self.close()
    
    def dialog_token(self):
        config = data.read_file()
        token = config.get('token')
        if token:
            text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter token:', 0, token)
        else:
            text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter token:')
        if ok:
            if text:
                logger.debug('token is inputed')
                if extra_tokencheck.check(self, text):
                    config['token'] = text
                    data.save_file(config)
                    token = config.get('token')
                    logger.info('token is valid from input')
                else:
                    logger.error('token is invalid from input')
            else:
                logger.warning('text is none')


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('Fusion')
    window = Base()
    app.exec_()
    logger.info('Shuting down app')
    logger.info('-------------------------------')
    sys.exit()