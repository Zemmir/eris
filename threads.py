from PyQt5 import uic, QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import *
import time

class Whitelist_th(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        print('Whitelist thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        print('Whitelist thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        self.doContinue = True
        self.started.emit()
        try:
            while self.doContinue:
                print('whitelist loop simulation')
                for _ in range(5):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            print("stop")
        print("thread is done")